// Require JS  Config File
require( {
			 baseUrl: '/js/',
			 paths: {
				   'angular': '../lib/angular/angular'
				 , 'angular-resource': '../lib/angular-resource/index'
				 , 'angular-route': '../lib/angular-route/index'
				 , 'angular-cookies': '../lib/angular-cookies/index'
				 , 'angular-mocks': '../lib/angular-mocks/angular-mocks'
				 , 'angular-sanitize': '../lib/angular-sanitize/index'
				 , 'angular-animate': '../lib/angular-animate/index'
				 , 'angular-touch': '../lib/angular-touch/index'
				 , 'jquery': '../lib/jquery/dist/jquery'
				 , 'Slidebars': '../lib/Slidebars/distribution/0.9.4/slidebars'
			 }
			 , map: {
				 '*': { 'jquery': 'jquery' }, 'noconflict': { "jquery": "jquery" }
			 }
			 , shim: {
				 'app': { 'deps': [
					 'angular'
					 , 'angular-route'
					 , 'angular-resource'
					 , 'angular-sanitize'
					 , 'angular-animate'
					 , 'angular-cookies'
					 , 'angular-touch'
					 , 'angular-mocks'
				 ]}
				 , 'config': { 'deps': ['app'] }
				 , 'angular-route': { 'deps': ['angular']}
				 , 'angular-resource': { 'deps': ['angular'] }
				 , 'angular-cookies': { 'deps': ['angular'] }
				 , 'angular-mocks': { 'deps': ['angular'] }
				 , 'angular-sanitize': { 'deps': ['angular'] }
				 , 'angular-animate': { 'deps': ['angular'] }
				 , 'angular-touch': { 'deps': ['angular'] }
				 , 'Slidebars':{ 'deps': ['jquery'] }
				 , 'jquery': {
					 init: function ( $ ) {
						 return $.noConflict( true );
					 },
					 exports: 'jquery'
				 }
				 , 'routes': { 'deps': [
					 'app'
				 ]}
				 , 'directives/jarvisMenu': { 'deps': [
					 'app', 'jquery'
				 ]}
				 , 'controllers/ApplicationController': { 'deps': [
					 'app'
					 ,'Slidebars'
				 ]}
			 }
		 }
	, [
			   'require'
			 , 'config'
			 , 'routes'
			 , 'directives/jarvisMenu'
			 , 'controllers/ApplicationController'
		 ]
	, function ( require ) {
		return require(
			[
				'bootstrap'
			]
		)
	}
);
