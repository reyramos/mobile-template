'use strict';

/**
 * # ApplicationController
 *
 * Core application controller that includes functions used before we kickStart the application
 * The functions store within this files live outside of the ngView and are used as global function
 */
angular.module( 'app' ).controller(
	'ApplicationController',
	[    '$rootScope'
		, '$scope'
		, '$log'
		, '$timeout'
		, '$location'
		, function
			 (
				 $rootScope
				 , $scope
				 , $log
				 , $timeout
				 , $location
				 ) {

			/********************************************
		 * VARIABLES
		 * Description: All Local Vars, private within controller
		 ********************************************/
			// The rate at which the menu expands revealing child elements on click
		$.menu_speed = 235;
		$.search_speed = 700;

		$scope.jarvisMenu = {
			closedSign: '<i class="fa fa-plus-square-o"></i>',
			openedSign: '<i class="fa fa-minus-square-o"></i>'
		}

		/*
		 * APP DOM REFERENCES
		 * Description: Obj DOM reference
		 */

		$.root_ = $( 'body' );
		$.left_panel = $( '#left-panel' );

		/********************************************
		 * VARIABLES
		 * Description: Accessible within controller's DOM
		 ********************************************/
		var mySlidebars = new $.slidebars();

		$scope.toggleSidebar = function(){
			$scope.activeSlide = !$scope.activeSlide
			mySlidebars.toggle('left');
		}

		$scope.safeApply = function ( fn ) {
			var phase = this.$root.$$phase;
			if (phase == '$apply' || phase == '$digest') {
				if (fn && (typeof(fn) === 'function')) {
					fn();
				}
			} else {
				this.$apply( fn );
			}
		};

		$scope.$on("$routeChangeSuccess", function(next, current ){


		})

	}]
)
