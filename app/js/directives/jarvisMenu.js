/**
 * Created by redroger on 1/21/14.
 */

angular.module( 'app' ).directive
( 'jarvisMenu', function () {
	var directive =
	{
		link: function ( scope, element, attr ) {

			if (typeof jQuery === "undefined" && jQuery === null) {
				throw new DOMException("Missing JQuery");
			}

			var  el = $(element )
				,defaults = {
					accordion: 'true',
					speed: 200,
					closedSign: '[+]',
					openedSign: '[-]'
				};

			// Extend our default options with those provided.
			if( typeof(attr.options) != "undefined" && typeof(scope[ attr.options ]) != "undefined" )
			{
				var options = scope[ attr.options ]
				Object.keys ( options ).forEach ( function( key ) {
					defaults[key] = options[key]
				} );
			}

			//add a mark [+] to a multilevel menu
			el.find( "li" ).each( function () {
				if ($( this ).find( "ul" ).size() != 0) {
					//add the multilevel sign next to the link
					$( this ).find( "a:first" ).append( "<b class='collapse-sign'>" + defaults.closedSign + "</b>" );

					//avoid jumping to the top of the page when the href is an #
					if ($( this ).find( "a:first" ).attr( 'href' ) == "#") {
						$( this ).find( "a:first" ).click( function () {
							return false;
						} );
					}
				}
			});

			//open active level
			el.find( "li.active" ).each( function () {
				$( this ).parents( "ul" ).slideDown( opts.speed );
				$( this ).parents( "ul" ).parent( "li" ).find( "b:first" ).html( defaults.openedSign );
				$( this ).parents( "ul" ).parent( "li" ).addClass( "open" )
			} );

			el.find( "li a" ).on( 'click', function () {
				if ($( this ).parent().find( "ul" ).size() != 0) {

					if (defaults.accordion) {
						//Do nothing when the list is open
						if (!$( this ).parent().find( "ul" ).is( ':visible' )) {
							var parents = $( this ).parent().parents( "ul" );
							var visible = el.find( "ul:visible" );
							visible.each( function ( visibleIndex ) {
								var close = true;
								parents.each( function ( parentIndex ) {
									if (parents[parentIndex] == visible[visibleIndex]) {
										close = false;
										return false;
									}
								} );
								if (close) {
									if ($( this ).parent().find( "ul" ) != visible[visibleIndex]) {
										$( visible[visibleIndex] ).slideUp( defaults.speed, function () {
											$( this ).parent( "li" ).find( "b:first" ).html( defaults.closedSign );
											$( this ).parent( "li" ).removeClass( "open" );
										} );

									}
								}
							} );
						}
					}// end if
					if ($( this ).parent().find( "ul:first" ).is( ":visible" ) && !$( this ).parent().find( "ul:first" ).hasClass( "active" )) {
						$( this ).parent().find( "ul:first" ).slideUp( defaults.speed, function () {
							$( this ).parent( "li" ).removeClass( "open" );
							$( this ).parent( "li" ).find( "b:first" ).delay( defaults.speed ).html( defaults.closedSign );
						} );

					} else {
						$( this ).parent().find( "ul:first" ).slideDown( defaults.speed, function () {
							/*$(this).effect("highlight", {color : '#616161'}, 500); - disabled due to CPU clocking on phones*/
							$( this ).parent( "li" ).addClass( "open" );
							$( this ).parent( "li" ).find( "b:first" ).delay( defaults.speed ).html( defaults.openedSign );
						} );
					} // end else
				} // end if
			} )


		}
	};
	return directive;
} );